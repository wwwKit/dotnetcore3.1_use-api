﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blogs.Api.Model;
using CommonToolsCore.TokenTools;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Blogs.Api.Controllers
{
    /// <summary>
    /// 测试类
    /// </summary>
    [ApiController]
    [Route("[controller]/[action]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ITokenHelper _Token;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="logger"></param>
        /// /// <param name="tokenHelper"></param>
        public WeatherForecastController(ILogger<WeatherForecastController> logger,ITokenHelper tokenHelper)
        {
            _logger = logger;
            _Token = tokenHelper;
        }

        /// <summary>
        /// 获取数值
        /// </summary>
        /// <param name="userDto">账号</param>
        /// <returns></returns>
        [HttpPost]
        public IEnumerable<WeatherForecast> Get([FromBody]UserDto userDto)
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        /// <summary>
        /// afasdf
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public Task<string> GetString() 
        {
            return Task.FromResult<string>("zhangsan");
            //return "zhangsan";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Task<string> GetToken() 
        {
            var claims = new Claim[] { new Claim("id", "1") };
            return Task.FromResult(_Token.GenerateAccessToken(claims).AccessToken);
        }
    }
}
