using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blogs.Api.Unility.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using CommonToolsCore.TokenTools.TokenServiceExtension;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CommonToolsCore.TokenTools;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using CommonToolsCore.CacheTools;

namespace Blogs.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        private readonly string _CorsName = "AnyCors";

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddControllersAsServices();

            #region Cors
            services.AddCors(o =>
            {
                o.AddPolicy(_CorsName, t =>
                {
                    t.AllowAnyMethod();
                    t.AllowAnyHeader();
                    t.AllowAnyOrigin();
                });
            });
            #endregion

            #region JwtToken
            services.Configure<TokenConfig>(Configuration.GetSection("tokenConfig"));
            var token = Configuration.GetSection("tokenConfig").Get<TokenConfig>();

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(token.Secret)),
                    ValidIssuer = token.Issuer,
                    ValidAudience = token.Audience,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddMemoryCache();
            services.AddTransient<ICacheHelper, MemoryCacheHelper>();
            services.AddTransient<ITokenHelper, TokenHelper>();
            #endregion
            //services.AddBearTokenExtension(Configuration);
            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",   //版本 
                    Title = ".net core 接口文档-NetCore3.1",  //标题
                    Description = ".net core Http API v1"    //描述
                });

                c.CustomSchemaIds(type => type.FullName);
                c.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}/Blogs.Api.xml");//Project.API 为你的项目名称
                //添加对控制器的标签(描述)
                c.DocumentFilter<ApplyTagDescriptionFilter>();//显示类名
                // 可以解决相同类名会报错的问题
                //c.CustomSchemaIds(type => type.FullName);

                //添加Authorization
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Scheme = "bearer",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                        },
                        new List<string>()
                    }
                });
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //跨域
            app.UseCors(_CorsName);

            app.UseRouting();

            #region Swagger
            app.UseSwagger(c=> {
                c.RouteTemplate = "doc/{documentName}/swagger.json";//配置后，你的最终访问路径就就是 /api-doc/index.html
            });
            app.UseSwaggerUI(c =>
            {
                c.ShowExtensions();
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "doc";
                c.SwaggerEndpoint("v1/swagger.json", "项目名称 Api v1");
                c.DefaultModelsExpandDepth(-1);
            });
            #endregion

            //认证
            app.UseAuthentication();

            //授权
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
